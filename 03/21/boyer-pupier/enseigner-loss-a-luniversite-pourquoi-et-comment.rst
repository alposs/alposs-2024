.. index::
   pair: Intervenante; Fabienne Boyer
   pair: Intervenant; Aurélien Pupier
   ! Enseignement


.. _boyer_2024_03_21:
.. _pupier_2024_03_21:

===========================================================================================================
10h05 **Enseigner l’OSS à l’Université : pourquoi et comment ?** par Fabienne Boyer & Aurélien Pupier
===========================================================================================================


Introdcution
==============

Enseigner l’OSS à l’Université : pourquoi et comment L’Open
Source est aujourd’hui majeur pour les étudiants et pour la société
en général. Comment accompagner au mieux les étudiants dans cette prise
de conscience et dans cette démarche technique et sociétale ?

Il s’agit non seulement de favoriser l’usage d’outils et de solutions Open Source,
mais également et surtout de familiariser les étudiants en informatique
avec les techniques de développement et de contribution dans un contexte Open
Source.

La présentation portera sur la mise en place d’un nouveau cours sur
ces aspects à l’Université Grenoble Alpes (UGA), pour les étudiants de
la formation Master 2 Génie Informatique de l’UFR IM2AG.

Fabienne Boyer, Maître de Conférences à l’UGA, présentera le contexte pédagogique
associé à ce cours.

Mickael Istria et Aurélien Pupier, ingénieurs chez Red Hat, présenteront leur
démarche pédagogique dans la mise en oeuvre de ce nouveau cours.

Fabienne boyer, UGA, & Aurélien Pupier, Red Hat

La vidéo
==========

- https://video.echirolles.fr/w/1cQbwy4vhAQPhA3RvsxHu2

.. raw:: html

   <iframe title="Fabienne Boyer - Enseigner l’OSS à l’Université : pourquoi et comment ?" width="560" height="315" src="https://video.echirolles.fr/videos/embed/01a6dfd6-d730-438f-b6f9-c976770237f5" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Fabienne Boyer
===============

.. figure:: images/boyer.webp

   Fabienne Boyer

Contexte : faible connaissance de l'OSS au niveau des étudiant·e·s
===================================================================

.. figure:: images/contexte.webp

   Faible connaissance de l'OSS au niveau des étudiant·e·s

Mise en oeuvre
================

.. figure:: images/mise_en_oeuvre.webp

Session 2023-2024
=====================

.. figure:: images/session_oss_2023_2024.webp


