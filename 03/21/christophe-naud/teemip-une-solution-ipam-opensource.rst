.. index::
   pair: Intervenant; Christophe Naud
   pair: Logiciel; TeemIP
   pair: Entreprise; TeemIP
   pair: TeemIP; Christophe Naud
   pair: Github; TeemIP
   ! IP

.. _naud_2024_03_21:

=========================================================================
17h04 **TeemIP, une solution IPAM OpenSource** par Christophe Naud
=========================================================================

- https://github.com/TeemIP
- https://www.teemip.net/
- https://wiki.teemip.net/doku.php?id=start


Introduction
================

La gestion des adresses IPs au sein d ‘une DSI fait face à des problématiques
peu souvent résolues : comment partager l’information, gérer les demandes,
avoir des rapports, l’intégrer à la CMDB… en résumé : comment se
libérer des tableurs ?

TeemIp, logiciel open source basé sur le framework iTop, se propose de répondre
à l’ensemble de ces questions.

Depuis 2010, date de démarrage du projet, la solution s’est aussi enrichie d’extensions
orientées DDI (gestion du DNS et du DHCP), Réseau (développement de la CMDB )
et Data Center (gestion du câblage).

Christophe Naud, TeemIP


La vidéo
==========

- https://video.echirolles.fr/w/1Ce62umcvwFCDVJCjCPgnF

.. raw:: html

   <iframe title="Christophe Naud - TeemIP, une solution IPAM OpenSource" width="560" height="315" src="https://video.echirolles.fr/videos/embed/050ed735-1e4c-4e30-97f7-216e939b0bdd" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Christophe Naud
===================

.. figure:: images/christophe_naud.webp

TeemIP
===================

.. figure:: images/teemIP.webp


2010
===================

.. figure:: images/2010.webp


iTOP
===================

.. figure:: images/itop.webp

Conclusion
===================

.. figure:: images/conclusion.webp

