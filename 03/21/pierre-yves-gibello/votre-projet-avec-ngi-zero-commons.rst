.. index::
   pair: Intervenant; Pierre Yves Gibello
   pair: Entreprise; OW2
   pair: OW2; Pierre Yves Gibello
   ! Communs internet
   ! Financements


.. _gibello_2024_03_21:

=========================================================================
13h48 **Votre projet avec NGI Zero Commons**  par Pierre Yves Gibello
=========================================================================

- https://nlnet.nl/commonsfund/

Introduction
===============

NGI Zero Commons fait partie de l’initiative NGI (Next Generation Internet)
du programme Horizon de la Commission européenne.

Il propose un financement en cascade basé sur des appels à projets, ouvert à
tous.

NGI Zero Commons est le véhicule de NGI pour les 3 prochaines années : son objectif,
financer le futur d’un Internet européen et des communs numériques basés sur
l’open source, intègre une vision sociétale fondée sur la protection des
données privées, l’inclusivité, les libertés publiques, la sobriété et la
neutralité des réseaux.

Pierre-Yves Gibello, OW2.

La vidéo
==========

- https://video.echirolles.fr/w/rNmfnZYBb8oWZRcEa4x5pj

.. raw:: html

   <iframe title="Pierre-Yves Gibello - Votre projet avec NGI Zero Commons" width="560" height="315" src="https://video.echirolles.fr/videos/embed/d0ed287c-1241-4ccf-8bc6-94c4f39de280" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Pierre-Yves Gibello
=====================

.. figure:: images/pierre_yves_gibello.webp

NGI Zero Commons Fund
==========================

.. figure:: images/annonce_tableau.webp

Budget
=======

.. figure:: images/budget.webp

Types de projets
====================

.. figure:: images/types_projets.webp

Comment candidater
====================


.. figure:: images/candidater.webp

Nlnet et OW2
=================

- https://nlnet.nl/commonsfund/

.. figure:: images/nlnet.webp

Communs internet, "La réponse à l'échec du marché et la course en avant dystopique repose sur l'action collective et l'investissement public" (NLnet)
============================================================================================================================================================

.. figure:: images/communs_numeriques.webp


"La réponse à l'échec du marché et la course en avant dystopique
repose sur l'action collective et l'investissement public" (NLnet)

Définitions, "Les choses doivent être rendues aussi simples que possible mais pas plus" (Einstein)
========================================================================================================

.. figure:: images/definitions.webp

"Les choses doivent être rendues aussi simples que possible mais pas plus"
(Einstein)

Get easy funding up to 150.000 euros
========================================

.. figure:: images/easy_funding.webp
