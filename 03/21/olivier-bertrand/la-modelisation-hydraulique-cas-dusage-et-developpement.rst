.. index::
   pair: Intervenant; Olivier Bertrand


.. _bertrand_2024_03_21:

================================================================================================
10h39 **La modélisation hydraulique : cas d’usage et développements** par Olivier Bertrand
================================================================================================

Description
=============

- En courte introduction, une présentation d’Artelia (avec son origine
  il y a plus de 100 ans et une société précurseur dans la modélisation
  hydraulique.
  Le premier modèle numérique d’ingénierie aurait été construit par Sogreah,
  notre ancien nom).

- La modélisation hydraulique en une diapo avec le passage d’une problématique
  physique à des équations mathématiques à résoudre -> besoin de moyens de calcul (HPC
  High Performance Computing)
- Des exemples d’application pour montrer la diversité de nos interventions
  (avec des échelles de temps et d’espace très différentes, un ouvrage, une
  ville, un estuaire, une côte…)
- Les outils/logiciels : pourquoi avons-nous fait le choix de l’open
  source. Exemple du logiciel open source Telemac pour lequel nous collaborons
  depuis plus de 25 ans (d’abord comme revendeur puis comme co-développeur
  suite au passage en libre). Voir opentelemac.org
- D’autres exemples de collaborations autour de l’open source : le logiciel
  OpenFOAM pour lequel nous avons une thèse en cours, nous avons participer
  à l’organisation de la conférence FOAM-U sur Grenoble en 2023…
  nous avons une chaire hydraulique du nom d’Oxalia.
  Le développement d’utilitaires autour des rendus de modélisation (QGIS, NIMPHS…)


La vidéo
==========

- https://video.echirolles.fr/w/44F7PRdz8DRmjC2gCj5Tec

.. raw:: html

   <iframe title="Olivier Bertrand - La modélisation hydraulique&nbsp;: cas d’usage et développements" width="560" height="315" src="https://video.echirolles.fr/videos/embed/18cec121-22ea-42e1-b548-e69d6fb1c7a9" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Olivier Bertrand
=====================

.. figure:: images/olivier_bertrand.webp

Vision de l'Open source
==========================

.. figure:: images/vision.webp

