.. index::
   pair: Intervenant; Cyril Zorma
   pair: Entreprise; Probesys
   pair: Logiciel; AgentJ
   pair: AgentJ; Cyril Zorma
   pair: Probesys; Cyril Zorma

.. _zorman_2024_03_21:

=============================================================
17h35 **AgentJ - L'antispam Libre** par Cyril Zorman
=============================================================

- https://github.com/Probesys/agentj
- https://agentj.io/
- https://github.com/Probesys

Introduction
=================

Présentation du socle technique, de l’histoire de la création et de la roadmap
d’AgentJ.

Cyril Zorman, Probesys

La vidéo
==========

- https://video.echirolles.fr/w/iEnVCCbGtWPvXcTbY8FHy4

.. raw:: html

   <iframe title="Cyril Zorman - AgentJ - L'antispam Libre" width="560" height="315" src="https://video.echirolles.fr/videos/embed/8f0721cd-a2af-429b-b2be-800b1bdb778f" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Cyril Zorman
====================

.. figure:: images/cyril_zorman.webp


agentj, l'anti-spam libre
==============================

.. figure:: images/agentj.webp

Comment contribuer
==============================

.. figure:: images/contribuer.webp
