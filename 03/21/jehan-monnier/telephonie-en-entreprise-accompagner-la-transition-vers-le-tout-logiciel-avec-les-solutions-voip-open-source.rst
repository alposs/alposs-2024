.. index::
   pair: Intervenant; Jehan Monnier
   pair: Entreprise; Belledonne Communications

.. _monnier_2024_03_21:

=================================================================================================================================================
15h30 **Téléphonie en entreprise : accompagner la transition vers le tout logiciel avec les solutions VoIP open source** par Jehan Monnier
=================================================================================================================================================

- https://linphone.org/

Introduction
=================

L’environnement téléphonique en entreprise est en constante évolution,
avec un rapprochement notable cette dernière décennie entre les départements
IT et Télécom au sein des organisations.

Les téléphones IP matériels et les serveurs physiques sont progressivement
complétés, voire remplacés, par des postes de travail sur PC et des serveurs
virtualisés, ou même par des solutions offertes en mode SaaS par les fournisseurs
de service.

Le renforcement du télétravail requiert des solutions permettant la mobilité
des utilisateurs et amène de nouvelles problématiques de sécurisation
du réseau et des communications.

Enfin, l’essor des smartphones dans les années 2010 a rapidement conduit à
l’émergence d’applications de communication propriétaires pour le marché de
masse, qui pose de nouveaux défis aux développeurs de solutions de télécommunication.

Jehan Monnier, Belledonne Communications


La vidéo
==========

- https://video.echirolles.fr/w/5PqVErhxvnQDjRtyHrkH2u

.. raw:: html

   <iframe title="Jehan Monnier - Téléphonie en entreprise : comment accompagner la transition vers le tout logiciel" width="560" height="315" src="https://video.echirolles.fr/videos/embed/2703df5e-e622-46df-a7e0-c55cef43b5c2" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Jehan Monnier
=================

.. figure:: images/jehan_monnier.webp

Liens
=================

.. figure:: images/liens.webp

