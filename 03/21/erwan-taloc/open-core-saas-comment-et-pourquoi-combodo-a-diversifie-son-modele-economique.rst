.. index::
   pair: Intervenant; Erwan Taloc
   pair: Entreprise; Combodo
   pair: Combodo; Erwan Taloc
   pair: Logiciel; iTop


.. _taloc_2024_03_21:

================================================================================================================
11h20 **Open core, SaaS, comment et pourquoi Combodo a diversifié son modèle économique ?** par Erwan Taloc
================================================================================================================

- https://www.itophub.io/
- https://github.com/Combodo/iTop
- https://www.combodo.com/

Introduction
=============

Editrice d’iTop, logiciel ITSM (ITSM ou Information Technology Service Management)
open-source, Combodo s’est développée en diversifiant ses sources de revenus.

Erwan Taloc, CEO et co-fondateur de Combodo, revient sur ce qui a motivé ces
diversifications, les embûches mais aussi les succès rencontrés.

"Opter pour une approche Open Source pour nous était avant tout un moyen de
nous faire connaître dans un marché très concurrentiel. Cependant, avec le recul,
cette approche est aussi une formidable opportunité de s’entourer d’une communauté
dynamique qui pourra utiliser librement la solution ou encore suggérer des
développements et des propositions d’améliorations.

Quand tout n’est pas Open Source il faut absolument avoir une stratégie claire
pour définir ce qui reste libre (reversé dans la communauté) et ce qui est réservé à la
version commerciale de la solution.

En effet, on peut très bien pratiquer l’open-core sans perdre son fondement
open source.

Il existe autant de modèles open-core que d’entreprises qui le pratiquent.
L’open-core peut être très restrictif ou au contraire permettre un usage très
complet comme dans le cas d’iTop community.

Ce mode permet à Combodo d’apporter des capacités supplémentaires à nos clients
et ainsi de financer notre R&D dont les développements bénéficient en retour
à l’ensemble de la communauté.

Il y a beaucoup d’idées reçues sur le sujet des modèles économiques et de la
commercialisation de l’open-source.

On oppose par exemple souvent les modèles SaaS et open-source, mais selon nous
leur cohabitation est loin d’être impossible, tout en respectant les avantages
et philosophies respectifs des deux modèles.

Notre expérience nous amène à penser que cette combinaison peut même s’avérer
vertueuse.

La version SaaS nécessite des évolutions techniques qui enrichissent le code
coeur open source mais apporte également un modèle économique plus équitable
car lié à l’utilisation.

Ce mode de contribution ajusté à la consommation des clients permet d’augmenter
la capacité d’investissement de Combodo.

Depuis sa création, Combodo est passée d’une société de services autour d’un
produit open-source, iTop, à une société éditrice de logiciel.

Ce qui nous permet de nous dédier davantage à la conception d’un produit pour tous.

Erwan Taloc, Combodo


La vidéo
==========

- https://video.echirolles.fr/w/tGpvVKMSfFktxmsgsV9Rzx

.. raw:: html

   <iframe title="Erwan Taloc - Comment et pourquoi Combodo a diversifié son modèle économique ?" width="560" height="315" src="https://video.echirolles.fr/videos/embed/e04afca2-a8c2-4443-9af1-cb4ca51d000d" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Erwan Taloc
=============

.. figure:: images/erwan_taloc.webp

Choix de l'Open source en 2010
===================================

.. figure:: images/choix_open_source_2010.webp


Arrivée du SAAS en 2022
===================================

.. figure:: images/saas.webp


itop
=====

.. figure:: images/itop.webp
