.. index::
   pair: Intervenante; Amandine Demore
   pair: Echirolles; Amandine Demore


.. _demore_2024_03_21:

==============================================================
9h20 **Accueil par Amandine Demore, maire d'Echirolles**
==============================================================

- https://fr.wikipedia.org/wiki/%C3%89chirolles
- https://piaille.fr/@amandinedemore


.. figure:: images/amandine_demore.webp


.. figure:: images/amandine_et_responsable_numerique.webp


.. figure:: images/les_5_copyright.webp


La vidéo
==========

- https://video.echirolles.fr/w/vDeE3cAVFi6ohLyiGNABj1


.. raw:: html

   <iframe title="Inauguration d'AlpOSS 2024 par Amandine Demore, Maire d'Échirolles" width="560" height="315" src="https://video.echirolles.fr/videos/embed/f00bf4d2-27d5-475d-8d94-207df48a3970" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>


Présentation de l'édition par l'équipe
==========================================

- https://video.echirolles.fr/w/ptrUaez4Euw39Wd6p4yjGm
