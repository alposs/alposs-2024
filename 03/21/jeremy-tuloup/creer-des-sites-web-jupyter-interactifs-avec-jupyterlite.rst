.. index::
   pair: Intervenant; Jérémy Tuloup
   pair: Logiciel; Jupyter
   pair: Logiciel; JupyterLite
   pair: Entreprise; QuantStack
   pair: QuantStack; Jérémy Tuloup
   pair: JupyterLite; Jérémy Tuloup
   pair: github; alposs-2024
   pair: github; jtpio
   ! JupyterLite
   ! Python
   ! Site statique


.. _tuloup_202_03_21:

===========================================================================================
17h35 **Créer des sites web Jupyter interactifs avec JupyterLite** par Jérémy Tuloup
===========================================================================================

- https://github.com/jtpio
- https://github.com/jupyterlite/jupyterlite
- https://jupyterlite.github.io/demo/lab/index.html
- https://docs.jupyter.org/en/latest/
- https://github.com/jtpio/alposs-2024
- https://jtp.io/alposs-2024/

Introduction
================

Les notebooks Jupyter sont un outil open source très populaire dans les
domaines de la science des données et du calcul scientifique, permettant aux
utilisateurs de combiner du code, du texte et des éléments multimédia au
sein d’un même document.

L’écosystème Jupyter est vaste et complexe, avec différents projets
et bibliothèques fonctionnant ensemble pour permettre la programmation
interactive et la science des données. Dans un premier temps, nous explorerons
l’écosystème Jupyter, en passant en revue les principaux projets et
bibliothèques.

Nous verrons comment les différents travaux au sein de l’écosystème Jupyter
ont conduit au récent projet **JupyterLite : une distribution Jupyter fonctionnant
intégralement dans le navigateur sans composants côté serveur**.

**Un avantage significatif de cette approche réside dans la facilité de déploiement et
l’accessibilité au plus grand nombre**.

JupyterLite est désormais de plus en plus adopté par les enseignants et
plateformes éducatives en ligne pour fournir un outil facilement accessible
pour l’apprentissage de la programmation, l’analyse de données, le calcul
scientifique et bien plus encore !

Jérémy Tuloup, QuantStack

La vidéo
==========

- https://video.echirolles.fr/w/sKfkUoVC9i9pNQBSPBwJcN

.. raw:: html

   <iframe title="Jérémy Tuloup - Créer des sites web Jupyter interactifs avec JupyterLite" width="560" height="315" src="https://video.echirolles.fr/videos/embed/d8976d32-d7af-4bdb-941c-e63d398e6cd4" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>


Les slides
=============

- https://jtp.io/alposs-2024/


Jérémy Tuloup, créateur de JupyterLite
===========================================

- https://github.com/jtpio


.. figure:: images/jeremy_tuloup.webp


.. figure:: images/cv.webp


L'écosystème Jupyter est très vaste
===========================================

.. figure:: images/ecosysteme.webp

JupyterLite
===========================================


.. figure:: images/jupyterlite.webp

Jupyter
===========================================


.. figure:: images/jupyter.webp

JupyterLite schema
===========================================


.. figure:: images/jupyerlite_schema.webp


Technos
===========================================


.. figure:: images/technos.webp


Jupyter et Python dans le navigateur, pas de serveur Python, peut être hébergé comme site statique
======================================================================================================


.. figure:: images/navigateur.webp

Générateur de site statique
======================================================================================================


.. figure:: images/site_statique.webp

::

    pip install jupyterlite-core
    jupyter lite build


iframe
======================================================================================================


.. figure:: images/iframe.webp


Education
======================================================================================================


.. figure:: images/education.webp

Déploiement sur github pages mais aussi sur gitlab
======================================================================================================


.. figure:: images/deploiement_github.webp


Demo
======================================================================================================

- https://jupyterlite.github.io/demo/lab/index.html

.. figure:: images/demo.webp

Références
======================================================================================================

- https://docs.jupyter.org/en/latest/
- https://jtp.io/alposs-2024/

.. figure:: images/references.webp

