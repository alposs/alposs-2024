.. index::
   pair: Intervenant; Johan Richer
   pair: Entreprise; multi
   pair: Coopérative; multi


.. _richer_2024_03_21:

===============================================================================
15h34 **Une coopérative du numérique d'intérêt général** par Johan Richer
===============================================================================

- https://www.multi.coop

Introduction
====================

Une coopérative du numérique d’intérêt général, ça fait quoi ?

multi est une coopérative, basée à Paris et Grenoble, qui conçoit des services
numériques pour le secteur public.

Quels sont les avantages d’une coopérative pour les travailleuses et travailleurs du numérique ?

Comment vendre et faire du logiciel libre au sein des administrations ?

À travers l’exemple de multi, nous présenterons des acteurs, méthodes et idées
qui peuvent inspirer l’écosystème du bassin isérois et au-delà.

Johan Richer, Multi Coop

La vidéo
==========

- https://video.echirolles.fr/w/aetxtsudqKhwM2xrAEKzaC

.. raw:: html

   <iframe title="Johan Richer - Une coopérative du numérique d’intérêt général" width="560" height="315" src="https://video.echirolles.fr/videos/embed/4ac3e405-6c95-4f09-99f4-ec957e2e64ea" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Johan Richer
==================

- https://www.multi.coop/team?locale=fr&item=.%2Ftexts%2Fteam%2Fpeople%2FJohan-Richer.md

.. figure:: images/johan_richer.webp

Une coopérative du numérique d'intérêt général, ça fait quoi, ça sert à quoi et à qui ?
============================================================================================

.. figure:: images/ca_sert_a_quoi.webp


Fondation, en 2017 par des anciens d'Etalab et Easter-eggs
==============================================================

- https://www.multi.coop/blog?locale=fr&item=.%2Ftexts%2Fblog%2Fposts%2Fjailbreak-devient-multi-fr.md

.. figure:: images/fondation.webp

Seconde fondation
=======================

.. figure:: images/seconde_fondation.webp

Les CASOS (Cap Gemini, Atos, Sopra, Steria, Orange, SIA partners)
=====================================================================

.. figure:: images/les_cas_soces.webp

La voie est libre
=====================================================================

.. figure:: images/la_voie_est_lbre.webp






