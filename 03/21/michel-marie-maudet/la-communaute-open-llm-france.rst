.. index::
   pair: Intervenant; Michel Marie Maudet
   pair: Entreprise; Linagora
   pair: Michel Marie Maudet; Linagora
   pair: Model; Claire
   pair: Github; OpenLLM-Europe
   pair: OpenLLM-Europe ; Manifesto
   pair: OpenLLM-France ; Manifesto
   ! OpenLLM France

.. _maudet_2024_03_21:

=================================================================
14h00 **La communauté OpenLLM France** par Michel Marie Maudet
=================================================================

- https://github.com/mmaudet
- https://www.openllm-france.fr/
- https://github.com/OpenLLM-Europe
- https://github.com/OpenLLM-France
- https://github.com/OpenLLM-France/Manifesto
- https://youtu.be/wg3l3NxbnRk
- :ref:`llms:llms`

#LogicielsLibres #AlpOSS #AlpOSS-2024 #Echirolles #Grenoble #LLM #OpenLLM #OpenLLMFrance #Linagora #LLM4ALL #Lucie #JeanZay


Introduction
===============

La communauté OpenLLM France 🇫🇷 créée en juin 2023 fédère à ce jour,
un écosystème de près de plus 450 entités (laboratoires publics de recherche,
fournisseurs potentiels de données, acteurs technologiques spécialisés,
fournisseurs de cas d’usage…).

L’ambition de cette communauté est de construire des communs numériques d’IA
générative réellement Open Source et maitrisés de bout en bout.

La notion d’Open Source est à différencier du terme "Open Weights".

Pour respecter la définition de l’Open Source Initiative, il faut que les
modèles soient entraînés à partir de jeux de données "blanches" d’apprentissage
c’est-à-dire collectés de manière éthique, responsable, équitable et que ces
datasets soient publiés sous licence libre permettant l’audit complet et
l’évaluation.

OpenLLM France 🇫🇷 collabore dès à présent avec les comités Alliance et LangIA afin
de mettre à disposition bases de données valorisant de patrimoine français,
francophone et européen dans l’entraînement d’IA génératives.

La communauté est déjà démontré sa capacité "à faire" avec le modèle
CLAIRE sorti en octobre 2023 avec la publication de l’intégralité des jeux
de données (https://github.com/OpenLLM-France/Claire-datasets) qui ont servi
à son entrainement.

La communauté vient de lancer en janvier 2024 le pré entraînement “from scratch”
un nouvau modèle (200 000 heures sur la machine Jean JAY planifiées entre
janvier et avril 2024) dont l’objectif est de produire un premier LLM “français”
performant sur la base de données blanches récoltées en fin d’année 2023.

L’intervention lors de l’AlpOSS permettra de témoigner de l’avancement de nos
travaux et des stratégies qui peuvent être mises pour disposer de LLM multimodaux
Open Source et souverains.

Exemple d’intervention réalisée lors de l’OSXP : https://youtu.be/wg3l3NxbnRk.

Michel-Marie Maudet, Linagora


La vidéo
==========

- https://video.echirolles.fr/w/ad8XBRZxLLJzAeoJhB69eW

.. raw:: html

   <iframe title="Michel-Marie Maudet - La communauté OpenLLM France 🇫🇷" width="560" height="315" src="https://video.echirolles.fr/videos/embed/4a9413fb-000e-44b8-8f07-2c603f93d4c0"
       frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Michel-Marie Maudet
======================

.. figure:: images/marie_michel_maudet.webp

OpenLLM France
===============

.. figure:: images/openllm_france.webp


Open source
===============

.. figure:: images/open_source.webp

Claire
===============

- http://ww01.hugginface.co/Openllm-france

.. figure:: images/claire.webp

Lucie
===============

.. figure:: images/lucie.webp

LLM4ALL Model for hospitals and emergency services
=======================================================

- https://synalp.gitlabpages.inria.fr/llm4all/

.. figure:: images/llm_for_all.webp


OpenLLM Europe, One LLM to free them all ;-)
=======================================================

- https://github.com/OpenLLM-Europe
- https://github.com/OpenLLM-Europe/Manifesto
- https://github.com/OpenLLM-Europe/European-OpenLLM-Projects

.. figure:: images/europe.webp

