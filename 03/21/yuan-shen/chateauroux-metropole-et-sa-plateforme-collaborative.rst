.. index::
   pair: Intervenante; Yuan Shen
   pair: Silverpeas; Yuan Shen
   pair: Entreprise; Silverpeas
   pair: github ; Silverpeas


.. _shen_2024_03_21:

=================================================================================
14h47 **Châteauroux Métropole et sa plateforme collaborative** par Yuan Shen
=================================================================================

- https://www.silverpeas.com/
- https://github.com/Silverpeas

Introduction
================

Silverpeas est un éditeur de logiciel open-source spécialisé dans la plateforme
collaborative.

Nous souhaitons profiter de cette conférence pour vous parler d’un témoignage
utilisateur : après avoir testé par eux-mêmes la plateforme collaborative
libre de Silverpeas, la Métropole Châteauroux est venue à nous et restée
notre client depuis maintenant plus de 8 ans.

Yuan Shen, Silverpeas

La vidéo
==========

- https://video.echirolles.fr/w/eG1fxLbF6umNtaBXRZ8Hg3

.. raw:: html

   <iframe title="Yuan Shen - Châteauroux Métropole : comment l'open-source vous amène-t-il des clients ?" width="560" height="315" src="https://video.echirolles.fr/videos/embed/6edcb53c-756d-4743-a538-d4a24a8007b4" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Yuan Shen
=============

.. figure:: images/yuan_shen.webp

Silverpeas
=============

.. figure:: images/silverpeas.webp

