.. index::
   pair: Table ronde ; Modèles d’affaires, entreprises et collectivités

.. _table_ronde_2024_03_21:

============================================================================
10h57 **Table ronde  Modèles d’affaires, entreprises et collectivités**
============================================================================


Introdution
===============

Cette table ronde offre une plateforme d’échanges visant à approfondir la
compréhension des différents points de vue et intérêts des acteurs impliqués,
notamment les entreprises open source et les acteurs publics.

L’objectif premier est donc d’amorcer un dialogue constructif en vue de
présenter les différents modèles économiques choisis ainsi que leurs enjeux.

Les intervenants :

- Pierre-Daniel Corbineau (Mairie de Gières)
- Joël Proal (Conseil départemental)
- Élisa Nectoux (Belledonne Communications)
- Damien Accorsi (Algoo)
- Cyril Zorman (Probesys)

La vidéo
==========

- https://video.echirolles.fr/w/p9evoHvKxBv7KetGfa7mJf

.. raw:: html

   <iframe title="Table ronde - Modèles d’affaires, entreprises et collectivités" width="560" height="315" src="https://video.echirolles.fr/videos/embed/bb687692-6d2c-40b7-b5e4-7a4cd0b1c222" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Annonce de la table ronde
==============================

.. figure:: images/annonce.webp


Intervenante/intervenants
==============================

.. figure:: images/les_personnes.webp

