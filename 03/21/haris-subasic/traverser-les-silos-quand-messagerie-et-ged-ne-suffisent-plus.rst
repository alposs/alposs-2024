.. index::
   pair: Intervenant; Haris Subašić
   pair: Entreprise; Bonitasoft
   pair: Haris Subašić; Bonitasoft

.. _subasic_2024_03_21:

=============================================================================================
16h50 **Traverser les silos, quand messagerie et GED ne suffisent plus** par Haris Subašić
=============================================================================================


Introduction
==============

Le travail en silo représente l’une des causes majeures de l’échec
des projets numériques au sein des entreprises.

Comment optimiser la collaboration inter département en tenant compte de ces
frontières invisibles ?

Décloisonner l’entreprise est crucial pour fluidifier les échanges,
améliorer l’efficacité au quotidien et assurer une bonne adhésion des
collaborateurs pour atteindre les objectifs de l’entreprise.

En pratique, il existe des méthodologies et des solutions éprouvées pour combler le
vide et ainsi donner de la confiance pour tisser de meilleurs liens entre
les départements et les personnes.

La gestion de processus métiers vise à répondre aux différents objectifs :

- rendre compréhensible les façons d’opérer et clarifier les rôles et les
  responsabilités,
- encourager et simplifier la communication donnant l’accès aux données nécessaires pour
  prendre des bonnes décisions,
- capitaliser sur les systèmes et les savoir-faire existants,
- donner du sens au travail des collaborateurs mettant les activités dans la
  perspective de bout-en-bout ou bien de répondre aux besoins et attentes
  des clients.

Haris Subasic, Pre Sales Director chez Bonitasoft, vous partage les
clés de ces méthodologies applicables à votre entreprise.

Haris Subašić, Bonitasoft


La vidéo
==========

- https://video.echirolles.fr/w/cuKCQR7YQg1fnCQxpebmBp

.. raw:: html

   <iframe title="Haris Subašić - Traverser les silos, quand messagerie et GED ne suffisent plus" width="560" height="315" src="https://video.echirolles.fr/videos/embed/5d183df1-fb15-4da1-8292-cc47eb0d9755" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Haris Subašić
=============

.. figure:: images/haris_subasic.webp


Des défis partagés
========================

.. figure:: images/defis.webp



Les processus sont les roues de l'entreprise
================================================

.. figure:: images/roues.webp

