.. index::
   pair: Intervenant; Bruno Cornec
   pair: Intervenant; Frédéric Passeron
   pair: Logiciel; hackshack
   pair: Entreprise; HPE
   pair: Entreprise; Flossita
   pair: Flossita; Bruno Cornec
   pair: HPE; Frédéric Passeron
   pair: Logiciel; Jupyter
   pair: Logiciel; ansible
   Licence ; Apache V2.0
   Licence ; CC-BY-SA 4.0
   ! Licences

.. _cornec_2024_03_21:

=====================================================================================================
10h20 **Libérer une solution d'apprentissage en ligne, de l'idée à la réalité** par Bruno Cornec
=====================================================================================================

- https://developer.hpe.com/hackshack/


Introduction
=============

HPE a développé une solution d’apprentissage pour aider ses équipes
techniques, comme celles de ses partenaires à mieux maîtriser les API de ses
produits et d’outils libres (Voir: https://developer.hpe.com/hackshack/).

De façon à permettre à un futur ancien salarié HPE de continuer à contribuer
au projet, nous avons décidé de le rendre complètement Open Source.

Cette présentation expliquera comment passer d’un concept simple sur le papier au
traitement réel (Validation par un OSRB, réarchitecture du code pour permettre
de conserver une partie de données privée, automatisation, nettoyage du code,
…).

Frédéric Passeron, HPE & Bruno Cornec, Flossita


La vidéo
==========

- https://video.echirolles.fr/w/byrZG2TdKJoP6MVYzuH37G

.. raw:: html

   <iframe title="Bruno Cornec - Libérer une solution d'apprentissage en ligne, de l'idée à la réalité" width="560" height="315" src="https://video.echirolles.fr/videos/embed/558334ff-1b37-40a1-8260-1fe2c7bde914" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Bruno Cornec et Frédéric Passeron
====================================

.. figure:: images/cornec_passeron.webp

Bio Bruno Cornec
=====================

.. figure:: images/bio_cornec.webp

Logiciels Jupyter, ansible
=============================

.. figure:: images/jupyter_hub.webp


Licences Apache V2, CC-BY-SA 4
==================================

.. figure:: images/licences.webp

Merci
==================================

.. figure:: images/merci.webp




