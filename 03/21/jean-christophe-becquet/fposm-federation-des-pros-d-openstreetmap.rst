.. index::
   pair: Intervenant; Jean-christophe-becquet
   pair: Entreprise; Apitux
   pair: Apitux; Jean-christophe-becquet
   pair: Logiciel; openstreetmap


.. _becquet_2024_03_21:

======================================================================================
17h20 **FPOSM Fédération des Pros d'OpenStreetMap** par Jean-christophe-becquet
======================================================================================

- https://fposm.fr/


Introduction
================


Lancée en 2022, la FPOSM est l’aboutissement de plusieurs années
d’échanges entre les professionnels d’OpenStreetMap. Elle s’appuie sur
des valeurs partagées et inscrites dans notre charte éthique.

L’association compte aujourd’hui une vingtaine d’entreprises. Elle offre
un espace de confiance et de partage entre ses membres. Elle mène des actions
de sensibilisation, notamment autour de la licence ODbL. Elle représente les
pros auprès de la Fondation OpenStreetMap, des contributeurs et contributrices,
des collectivités et des entreprises.

La FPOSM est présente notamment au SotM (State Of The Map), la
rencontre annuelle des contributeurs et contributrices OpenStreetMap, aux
GéoDataDays. Nous souhaitons vivement participer à cette première édition
d’AlpOSS.

APITUX compte parmi les 9 membres fondateurs et Jean-Christophe Becquet est
vice-président de l’association.

Jean-Christophe Becquet, Apitux


La vidéo
==========

- https://video.echirolles.fr/w/3eDzQRVZV3MC7MnoSKxApJ

.. raw:: html

   <iframe title="Jean-Christophe Becquet - FPOSM Fédération des Pros d'OpenStreetMap" width="560" height="315" src="https://video.echirolles.fr/videos/embed/121a1ba4-7e84-4a8b-9885-35e25ce920f0" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Jean-Christophe Becquet
============================

.. figure:: images/jean_christophe_becquet.webp

Fédération
============================

.. figure:: images/fede.webp

Histoire
============================

.. figure:: images/histoire.webp


Commentaire de Jean-Christophe Becquet
==========================================

Bonjour,

Ma conférence sur la Fédération des pros d'OSM est disponible en ligne.
https://video.echirolles.fr/w/3eDzQRVZV3MC7MnoSKxApJ

La vidéo est partagée sous licence libre Creative Commons BY-SA et
hébergée sur une instance PeerTube.

J'en profite pour renouveler mes félicitations et remerciements les plus
chaleureux à l'équipe de d’organisation : Ville d’Échirolles, OW2 et
Belledonne Communications.

Et bien sûr rendez-vous en 2025 pour la prochaine édition AlpOSS,
l'événement isérois de l’écosystème Open Source.

Bonne journée

