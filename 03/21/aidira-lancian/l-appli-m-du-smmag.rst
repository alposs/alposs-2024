.. index::
   pair: Intervenant; Sidi-Mohamed Aidira
   pair: Intervenant; Christophe Lancian
   pair: Entreprise; Sully group
   ! Eco-conception
   ! Loi REEN


.. _aidira_2024_03_21:
.. _lancian_2024_03_21:

===============================================================================
15h02 **L'appli M du SMMAG** par Sidi-Mohamed Aidira & Christophe Lancian
===============================================================================

Christophe Lancian et Sidi-Mohamed Aidira
==============================================

.. figure:: images/aidira_lancian.webp


La vidéo
==========

- https://video.echirolles.fr/w/9hSBfq3evQzzPbZEsdNfzo

.. raw:: html

   <iframe title="Sidi-Mohamed Aidira &amp; Christophe Lancian - L'appli « M »" width="560" height="315" src="https://video.echirolles.fr/videos/embed/4324329e-03f6-4cf2-84a1-d4616dda41b8" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Introduction
==================

**L’Appli M**, application phare de services numériques multimodaux (ou
MaaS pour Mobility as a Service).

L’appli M permet aux usagers :

- de trouver les meilleurs itinéraires en transport en commun, covoiturage, autopartage,
  vélos, trottinettes…
- de connaître les horaires et les perturbations en temps réel ;
- de gérer le stationnement dans les parkings du cœur urbain ;
- d’acheter les titres de transports et les abonnements.

Il ne passe pas une journée sans lire que nos enjeux de ressources
énergétiques commencent aujourd’hui par la réflexion et surtout la
mise en œuvre de meilleures pratiques IT, devant nous aider à réduire
notre consommation énergétique.

Si répondre à des enjeux RSE est dans l’ère du temps, c’est bien parce que
nous nous devons de changer nos usages et de réfléchir collectivement sur
les bonnes façons d’y arriver.

L’écoconception est l’affaire de tous les acteurs IT.

Présentation des 8 thématiques du RGESN (Référentiel général
d’écoconception de services numériques): Stratégie Spécifications
Architecture UX/UI Contenus Frontend Backend Hébergement

Quel impact en termes de développement et de suivi de projet pour les équipes dédiées au SMAAG ?
===================================================================================================

Témoignage de Sidi Mohamed, un des développeur concepteur de la solution
“M” qui nous partagera son retour d’expérience :

- Passer en revue les critères du RGESN Pour chaque règle du RGESN, croiser les critères
  d’acceptation avec la ou les bonnes pratiques des 115 Bonnes Pratiques de
  GreenIT. Ces bonnes pratiques sont souvent plus unitaires et plus proches de
  la technique que du métier.  - Énoncer un ensemble de recommandations
  en précisant leur impact et en évaluant le coût de leur mise en œuvre.
- Prendre une photo » du système actuel afin de pouvoir comparer la
  performance avant et après l’implémentation des recommandations. Pour le
  frontend, l’outil Lighthouse de Google permet une telle comparaison assez
  rapidement.
  Côté backend, nous avons utilisé Locust, un outil Python qui permet de créer
  des scénarios d’utilisation de notre serveur pour réaliser de véritables
  tests de charge.

Concrètement, il est essentiel de s’assurer d’une mise en œuvre technique
appropriée des différentes couches de l’application : Pour le frontend,
on cherchera les axes d’amélioration tels que le temps nécessaire pour
que le navigateur restitue le premier élément de contenu DOM, le temps
nécessaire pour que la page devienne entièrement interactive ou encore
le temps total bloquant de toutes les longues tâches lors de la navigation
vers l’application.

Il est nécessaire de compléter cette analyse par une
analyse manuelle, qui implique notamment d’examiner, entre autres, le temps
de manipulation du DOM, la taille du javascript, le temps d’exécution du
javascript.

Pour le backend, il s’agit principalement de minimiser la charge
du serveur. Par exemple, pour les données en temps réel, il est préférable
d’adopter un système de push du serveur vers le ou les clients, ce qui est
plus adapté à l’écoconception.

Il est également important de mettre en
place une architecture correspondant aux besoins métiers (les microservices
sont excellents dès qu’on se retrouve dans des situations complexes).

Il existe plusieurs patterns pour optimiser la sollicitation des serveurs logiques
(Circuit Breaker, Retry Pattern…).

La gestion des données est également
importante : il convient de mettre en place un système de cache robuste et
d’éviter les requêtes dans les boucles…

Finalement, ce qu’il faut surtout retenir pour un Concepteur/Développeur,
c’est qu’on fait naturellement de l’éco-conception en appliquant systématiquement
les bonnes pratiques de la conception d’un système informatique.

Sidi-Mohamed Aidira & Christophe Lancian,
Sully Group

Open source et éco-conception, loi REEN
==============================================

- https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000044327272
- https://label-nr.fr/decryptage-loi-reen/

Le 15 novembre 2021, la loi visant à réduire l’empreinte environnementale du
numérique (REEN) a été officiellement promulguée, une première mondiale.

Issue d’une proposition de loi déposée par les sénateurs Guillaume Chevrollier
et Jean-Michel Houllegatte, la loi REEN propose une série de mesures pour
favoriser un numérique sobre, responsable et écologiquement vertueux en France.

Si les ambitions du texte ont été revues à la baisse, notamment sur les
obligations pensant sur les producteurs, la loi REEN va dans le bon sens
pour réduire l’impact environnemental du numérique en France.



.. figure:: images/eco_coception.webp




