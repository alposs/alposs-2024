.. index::
   pair: Intervenant; Pascal Kuczynski
   pair: Entreprise; Adullact! SSO
   pir: Logiciel ; France Connect

.. _kuczynski_2024_03_21:

=============================================================
15h19 **Démarches Simplifiées** par Pascal Kuczynski
=============================================================

- https://www.demarches-simplifiees.fr/
- https://doc.demarches-simplifiees.fr/

Introduction
=================

Exposer la gestion d’une communauté du libre, au travers du projet Démarches Simplifiées.

Ce projet est initialement (et encore aujourd’hui) développé par des développeurs
de l’État qui n’ont peut-etre pas le meme niveau de conscience de l’intéret
d’une communauté autour d’un logiciel libre qu’on souhaite partager et faire vivre.

Pascal Kuczynski, Adullact

Description
================

« Démarches simplifiées » permet de dématérialiser des démarches administratives
grâce à un générateur de formulaires et une plateforme d'instruction de dossiers.

Il s'agit d'une application en ligne prête à l'emploi développée, hébergée et
maintenue par la Direction Interministérielle du Numérique (DINUM), mise à
disposition de l'ensemble des organismes publics.

Elle est interconnectée à de nombreux services de l'État, notamment France Connect,
API Entreprise, API Géo et BAN.


La vidéo
==========

- https://video.echirolles.fr/w/6TstVpEseCfdTseCUh1LCB

.. raw:: html

   <iframe title="Pascal Kuczynski - Démarches simplifiées" width="560" height="315" src="https://video.echirolles.fr/videos/embed/2facf1ff-be31-4f28-86fd-3daa7ab4717b" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

.. images/adullact.webp

Pascal Kuczynski
====================

.. figure:: images/pascal_kuczynski.webp

Adullact
====================

.. figure:: images/adullact.webp

Adullact, animateur de communautés
=======================================

.. figure:: images/communautes.webp

Services en ligne: pki, démarches simplifiées
=================================================

.. figure:: images/services_en_ligne.webp


Un outil, ajout France Connect multi-jetons, SSO
=====================================================

.. figure:: images/services_en_ligne.webp


Histoire
=============

.. figure:: images/histoire.webp


Communaute
=================

.. figure:: images/communaute.webp

Développements
====================

.. figure:: images/dev.webp
