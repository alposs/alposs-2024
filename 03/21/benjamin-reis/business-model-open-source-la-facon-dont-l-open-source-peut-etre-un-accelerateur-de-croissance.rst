.. index::
   pair: Intervenant; Benjamin Reis
   pair: Entreprise; Vates
   pair: license ; GPLv2
   pair: license ; aGPL
   pair: github ; vates
   pair: Logiciel ; xcp-ng

.. _reis_2024_03_21:

=====================================================================================================================================
11h50 **Business model Open Source : la façon dont l'Open Source peut être un accélérateur de croissance** par Benjamin Reis
=====================================================================================================================================

- https://github.com/vatesfr

Introduction
==============

Dans cette conférence, nous partagerons notre expérience chez Vates sur
l’adoption d’un modèle open source comme catalyseur de croissance.

Nous discuterons de notre approche pour développer une entreprise rentable
en s’appuyant sur les principes de l’open source, non seulement en mettant
à disposition notre code librement, mais en adoptant une transparence à tous
les niveaux organisationnels.

L’accent sera mis sur l’importance de bâtir une culture d’entreprise autour
de l’open source pour permettre une véritable participation communautaire
et transformer cette philosophie en un véritable levier de croissance.

Benjamin Reis, Vates


La vidéo
==========

- https://video.echirolles.fr/w/9hzZ8yopd99VQrRQLiBTtx

.. raw:: html

   <iframe title="Benjamin Reis - Comment l'Open Source peut être un accélérateur de croissance" width="560" height="315" src="https://video.echirolles.fr/videos/embed/4319f41f-5ef6-49ae-8e27-69924966e8f1" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>


Benjamin Reis
================

::

    benjamin.reis@vates.tech


.. figure:: images/benjamin_reis.webp


Vates
=========

- https://xcp-ng.org/
- https://www.youtube.com/@Vates_tech/videos


.. figure:: images/vates.webp


Logiciel xcp-ng
===================

- https://docs.xcp-ng.org/
- https://docs.xcp-ng.org/category/project/
- https://xcp-ng.org/blog/
- https://xcp-ng.org/forum/

xcp-ng versus VMWare
-------------------------

.. figure:: images/versus_vmware.webp

   https://docs.xcp-ng.org/#compared-to-vmware

Metier, licenses (GPLv2, Affero GPL)
========================================

- https://fr.wikipedia.org/wiki/GNU_Affero_General_Public_License

.. figure:: images/metier.webp

