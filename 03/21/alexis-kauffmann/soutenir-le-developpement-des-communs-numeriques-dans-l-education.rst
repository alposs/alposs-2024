.. index::
   pair: Intervenant; Alexis Kauffmann
   pair: Ministère Education; Alexis Kauffmann
   pair: Logiciel; Capytale
   ! Framasoft
   ! Education

.. _kauffman_2024_03_21:

===================================================================================================
9h40 **Soutenir le développement des communs numériques dans l'éducation** par Alexis Kauffmann
===================================================================================================

- https://fr.wikipedia.org/wiki/Alexis_Kauffmann
- https://framasoft.org/fr/

#LogicielsLibres #AlpOSS #AlpOSS-2024 #Echirolles #Grenoble #Education #Framasoft #CommunsNumériques

Introduction
===============

Pour la première fois le ministère de l’Éducation nationale intègre les communs
numériques dans sa stratégie et la volonté affichée de les soutenir et de
les développer.

Pourquoi et comment ? Quels sont les enjeux et les défis rencontrés ?

Une intervention d’`Alexis Kauffmann <https://fr.wikipedia.org/wiki/Alexis_Kauffmann>`_, fondateur de `Framasoft <https://framasoft.org/fr/>`_ et chef de projet
logiciels et ressources éducatives libres à la Direction du numérique pour
l’éducation du ministère de l’Éducation nationale et de la Jeunesse.


Biographie
==============

- https://fr.wikipedia.org/wiki/Alexis_Kauffmann

Alexis Kauffmann, né le 5 mars 1969 à Boulogne-Billancourt (Hauts-de-Seine),
est un professeur de mathématiques et militant libriste français.

En 2001, il est à l'origine de Framasoft, l'une des principales communautés
de promotion et de diffusion du logiciel libre et de la culture libre en France,
qui se constitue en 2004 en association dont il est le cofondateur et le
premier président de 2004 à 2012.

En 2021, il rejoint la direction du numérique pour l'éducation en tant que
chef de projet logiciels et ressources éducatives libres.

La vidéo
==========

- https://video.echirolles.fr/w/9PwheMu7Duh1ndV2FTZNmF

.. raw:: html

   <iframe title="Alexis Kauffmann - Soutenir le développement des communs numériques dans l'éducation" width="560" height="315" src="https://video.echirolles.fr/videos/embed/476bba27-07ce-403a-9b2f-85cea4859a5f" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Alexis Kauffmann
==================

.. figure:: images/kauffmann.webp

Ecole inclusive
=====================

.. figure:: images/ecole_inclusive.webp


Le logiciel capytale
========================

- https://pia.ac-paris.fr/portail/jcms/p2_1986518/un-nouvel-outil-pour-la-programmation-python-au-lycee-capytale

.. figure:: images/capytale.webp

   https://capytale2.ac-paris.fr/web/c-auth/list

Logiciels
==========

- https://drane.edu.ac-lyon.fr/spip/ELEA-la-plateforme-Moodle-de-l


