.. index::
   pair: Intervenant; Philippe Scoffoni
   pair: Intervenante; Marie-Jo KOPP CASTINEL
   pair: Format ; PDF
   pair: Format; ODF
   pair: Format; Oxml
   ! Formats


.. _kopp_2024_03_21:
.. _scoffoni_2024_03_21:

=============================================================================================================================================
12h00 **2024, où en sommes-nous des solutions bureautiques libres et collaboratives ?** par Marie-Jo KOPP CASTINEL & Philippe Scoffoni
=============================================================================================================================================


Introduction
=================

État des lieux sur les suites bureautiques collaboratives libres.

Présentation des écarts fonctionnels et de support entre LibreOffice et OnlyOffice,
des enjeux autour du format des documents.

Marie-Jo KOPP CASTINEL & Philippe Scoffoni

La vidéo
==========

- https://video.echirolles.fr/w/aTvC2t1mPYVRYr8GrW6MW6

.. raw:: html

   <iframe title="Marie-Jo Kopp, Philippe Scoffoni - 2024, ou en sommes-nous des solutions bureautiques libres et collaboratives&nbsp;?" width="560" height="315" src="https://video.echirolles.fr/videos/embed/50136f54-190b-4cab-9ee0-fbd4856c94dd" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Marie-Jo KOPP CASTINEL & Philippe Scoffoni
=============================================

.. figure:: images/knop_scoffoni.webp


Outil et format
=================

.. figure:: images/outil_et_format.webp

Ecarts fonctionnels
======================

.. figure:: images/ecarts.webp

Ecarts techniques
======================

.. figure:: images/ecarts_techniques.webp

