.. index::
   pair: Intervenant; Pierre Carlier
   pair: Entreprise; BlueMind

.. _carlier_2024_03_21:

=============================================================================
14h17 **Migration de sa messagerie en Open source** par Pierre Carlier
=============================================================================


Introduction
================

Concrétiser la migration de sa messagerie en Open source sans compromis sur les
habitudes de vos utilisateurs.

Dans le domaine de la messagerie électronique, de la théorie (l’Open Source
c’est bien, on va prendre Thunderbird ou passer en web) à la pratique (on migre réellement)
il y a un univers : les usages et les habitudes !

De par ses fonctionnalités avancées, la solution Outlook / Exchange
(ou 365) de MS, a ancré des habitudes et besoins aux utilisateurs dont ils ne
veulent plus se passer.

Bonne nouvelle, une messagerie open source française, BlueMind, réconcilie
la volonté d’Open Source et souveraineté avec la satisfaction des utilisateurs.

Cette conférence présentera comment BlueMind, grace à la maitrise de l’ensemble
de la solution, a réussi l’exploit d’être la seule solution à supporter Outlook
nativement.

Le support de certaines fonctionnalités avancées apportées par Outlook sera détaillé,
ainsi que leur implémentation pour les rendre également disponibles via
Thunderbird, le web et les mobiles !

Outlook, Thunderbid, web, mobiles, avec BlueMind tout est permis, sans compromis,
avec le plus haut niveau de fonctionnalités et collaboration.

Ceci ouvre la possibilité de différents scénarios de migration qui seront
présentés, et qui avec la présence d’un éditeur permet de lever les freins
et réellement concrétiser la migration de votre messagerie vers une solution
Open Source.

Pierre Carlier, BlueMind

La vidéo
==========

- https://video.echirolles.fr/w/bBVKfDqh9fkW8HeWuA4wYa

.. raw:: html

   <iframe title="Pierre Carlier - Concrétiser la migration de sa messagerie en Open source" width="560" height="315" src="https://video.echirolles.fr/videos/embed/55ff8a60-7942-443e-84e8-f2751d9ca809" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Pierre Carlier
=================

.. figure:: images/pierre_carlier.webp

Nous voulons outlook !!!
============================

.. figure:: images/pas_changer.webp


Multiclient
============================

.. figure:: images/multi_client.webp

