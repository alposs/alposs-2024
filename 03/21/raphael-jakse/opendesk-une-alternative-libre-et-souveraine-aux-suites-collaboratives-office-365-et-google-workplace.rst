.. index::
   pair: Intervenant; Raphaël Jakse
   pair: Logiciel; Opendesk

.. _jakse_2024_03_21:

=======================================================================================================================================
14h32 **OpenDesk - Une alternative libre et souveraine aux suites collaboratives Office 365 et Google Workplace** par Raphaël Jakse
=======================================================================================================================================

- https://xwiki.com/en/Blog/XWiki-joins-OpenDesk/
- http://www.univention.com/
- http://www.openproject.org/

Introduction
==================

OpenDesk, financé initialement par le ministère de l’Intérieur allemand
et organisé par Dataport / Bechtle / BMI / PWC, est une coopération
entre plusieurs acteurs publics et privés pour construire une plateforme
interopérable, intégrée, unifiée, 100% Open Source, intégrant des outils de
productivité, collaboration, communication, certifiée C5 pour le déploiement
au sein du gouvernement allemand.

Le projet regroupe aujourd’hui des éditeurs de solutions libres réputés:

- OpenExchange,
- NextCloud,
- Element / Jitsi,
- `XWiki <https://xwiki.com/en/Blog/XWiki-joins-OpenDesk/>`_
- Collabora,
- CryptPad,
- `OpenProject <http://www.openproject.org/>`_
- `Univention (gestion des utilisateurs, groupes et portail d’accès aux services) <http://www.univention.com/>`_

Il est important de remplacer l’utilisation ultra-répandue, y compris
dans nos administrations, des outils propriétaires proposés par les géants
américains par des solutions sur lesquelles on peut garder le contrôle pour
des raisons de souveraineté et de vie privée.

openDesk se présente comme solution cohérente et soutenue par des acteurs
européens et locaux.

Nous donnerons un historique du projet, évoquerons les acteurs impliqués et
leurs interactions, présenterons ses principes techniques fondamentaux,
ses fonctionnalités et ses composantes. Raphaël Jakse, XWiki

La vidéo
==========

- https://video.echirolles.fr/w/hjUGjWtss4wiEXYq5nemsa

.. raw:: html

   <iframe title="Raphaël Jakse - OpenDesk (Sovereign Workplace Government)" width="560" height="315" src="https://video.echirolles.fr/videos/embed/843614bf-3e29-4583-a918-0bbb730a6295" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>


Raphaël Jakse
===================

.. figure:: images/raphael_jackse.webp

Qui suis-je ?
===================

- https://xwiki.com/en/Blog/XWiki-joins-OpenDesk/

.. figure:: images/qui_suis_je.webp


Contexte : google workplace, microsoft Office 365
========================================================

.. figure:: images/contexte.webp


Normes: directive DINUM contre microsoft Office 365, ANSSI SecNumCloud, BSI C5, ESCloud
================================================================================================


.. figure:: images/normes.webp


Choix politique de l'Allemagne
======================================


.. figure:: images/allemagne.webp

Acteurs clés
=================

.. figure:: images/acteurs_cles.webp

Fonctionnalités
=================

.. figure:: images/fonctionnalites.webp


Fonctionnement: docker, univention
========================================

.. figure:: images/fonctionnalites.webp


En pratique
========================================

.. figure:: images/pratique.webp
