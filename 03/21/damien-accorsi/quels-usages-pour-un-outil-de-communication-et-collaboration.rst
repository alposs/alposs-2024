.. index::
   pair: Intervenant; Damien Accorsi
   pair: Algoo; Damien Accorsi
   pair: Entreprise; Algoo
   pair: Logiciel; Tracim
   pair: Tracim; Damien Accorsi

.. _accorsi_2024_03_21:

==============================================================================================
16h32 **Quels usages pour un outil de communication et collaboration ?** par Damien Accorsi
==============================================================================================

- https://fosstodon.org/@Lebouquetin (Damien Accorsi)
- https://framapiaf.org/@tracimfr
- https://www.algoo.fr/fr/
- https://www.tracim.fr/fr/
- https://github.com/tracim/tracim


Introduction
===============

L’idée de la conférence et est présent la démarche produit autour de
tracim et de la confronter avec la réalité des usages qui en sont faits.

On parlera donc des usages réels faits par nos clients et utilisateurs, qui sont
parfois proches de ce qu’on conçoit et parfois très éloignés.

Un parallèle sera également fait avec des outils plus  techniques pour
finalement conclure sur l’importance de l’appropriation de l’outil
(qu’il s’agisse de collaboration ou autre) par les équipes.

Damien Accorsi, Algoo


La vidéo
==========

- https://video.echirolles.fr/w/b2GpaGsjxuPvRypjHABv2K

.. raw:: html

   <iframe title="Damien Accorsi - Quels usages pour un outil de communication et collaboration d'équipe ?" width="560" height="315" src="https://video.echirolles.fr/videos/embed/51380704-1403-4459-bfec-f5cdeb134bb1" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

Damien Accorsi
=================

.. figure:: images/damien_accorsi.webp

Un outil pour quoi faire ?
================================

.. figure:: images/pour_quoi_faire.webp


Défis
================================

.. figure:: images/defis.webp

Bases documentaires
================================

.. figure:: images/bases_documentaires.webp


Animation de communauté
================================

.. figure:: images/animation.webp

Le bon outil
================================

.. figure:: images/bon_outil.webp



