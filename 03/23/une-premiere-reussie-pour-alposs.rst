
.. _news_2024_03_23:

=============================================================
2024-03-23 Article **Une première réussie pour AlpOSS !**
=============================================================

- https://www.echirolles.fr/actualites/une-premiere-reussie-pour-alposs


Introduction
=================

Ce jeudi 21 mars s’est tenu la 1ère édition d’AlpOSS au sein de
l’Hôtel de Ville. Coorganisé avec OW2 et Belledonne Communications, le
nouvel événement isérois de l’écosystème open source local a réuni 250
personnes - entreprises, associations, collectivités locales ou passionné-es
du libre - venu-es se rencontrer et échanger.

AlpOSS, pour "Open Source Software" : c’est un nouvel événement autour
du logiciel libre qui s’est déroulé tout au long de la journée du jeudi 21
mars au sein de l’Hôtel de Ville, inauguré par la Maire Amandine Demore. Au
programme : des ateliers et des conférences avec différent-es intervenant-es
venu-es de tous horizons, ainsi que des temps dédiés d’échanges avec les
exposant-es et les participant-es, le tout diffusé en direct sur Peertube.

Une première inédite dans le bassin grenoblois
=====================================================

Une première pour la Ville et ses partenaires, mais aussi une première pour
le territoire, qui ne comptait à ce jour pas d’événement de cette ampleur
sur l’open source.

Aurélien Farge, adjoint au développement du numérique
libre décrit "un élan autour du logiciel libre qui touche non seulement
les collectivités territoriales, mais aussi de nombreuses entreprises et
acteurs locaux : la création de synergies était importante". Nicolas
Vivant, directeur de la stratégie numérique et de la culture numériques
à Échirolles, confirme cet élan : "nous avons entendu parler d’autres
acteurs qui voulaient organiser quelque chose, alors on s’est lancé pour
le faire ensemble".

L’open source et le numérique libre, un travail de longue haleine pour Échirolles
======================================================================================

En novembre 2021, "le schéma directeur du numérique libre à Échirolles
était voté en conseil municipal : depuis, la promotion des logiciels libres et
la mise en œuvre d’une véritable souveraineté numérique à l’échelon
local ont été mis en œuvre" déclarait Amandine Demore lors du discours
d’inauguration de la journée.

Un engagement récompensé par le succès de cette journée, mais pas que :
la Maire ainsi que son adjoint Aurélien Farge se sont vus remettre la plaque
"Territoire Numérique Libre" de niveau 5 par l’Adullact, la plus haute
distinction du label.

Cette dernière fera son apparition aux différentes entrées de la Ville.

Une "reconnaissance pour toutes les équipes qui travaillent au quotidien pour
porter nos valeurs sur le numérique libre", a affirmé Aurélien Farge.

De quoi, peut-être, amener à une reconduction de ce temps fort l’année prochaine !


Pour en savoir plus
==========================

- `Sur les logiciels open source et libres <https://www.echirolles.fr/la-ville/echirolles-territoire-numerique/logiciels-libres>`_
- `Sur le label Territoire Numérique Libre <https://www.echirolles.fr/actualites/territoire-numerique-libre-echirolles-decroche-le-plus-haut-niveau-de-labellisation>`_
- `Sur l'ensemble des partenaires, participants et sponsors d'AlpOSS <https://alposs.fr/>`_



