.. index::
   ! AlpOSS 2024

.. raw:: html

    <a rel="me" href="https://babka.social/@pvergain"></a>
    <a rel="me" href="https://framapiag.org/@pvergain"></a>

.. un·e

|FluxWeb| `RSS <https://alposs.frama.io/alposs-2024/rss.xml>`_


.. _alposs_2024:

================================================
**Alpes Open Source Software (AlpOSS) 2024**
================================================

- https://alposs.fr/
- https://alposs.fr/wp-content/uploads/2024/03/Programme-AlpOSS-2024.pdf
- https://www.instagram.com/villedechirolles/
- https://colter.social/@alposs
- :ref:`metro_grenoble_libre:alposs_2024`
- https://jtp.io/alposs-2024/
- https://video.echirolles.fr/
- https://video.echirolles.fr/alposs
- https://video.echirolles.fr/feeds/videos.xml?videoChannelId=11 (Flux Web RSS)
- https://video.echirolles.fr/c/numerique_libre/videos

#LogicielsLibres #AlpOSS #AlpOSS-2024 #Echirolles #Grenoble

:download:`Télécharger le programme au format PDF <03/21/pdfs/Programme-AlpOSS-2024.pdf>`

Nouvel événement de l’écosystème open source local et régional, AlpOSS **Alpes Open Source Software**
s’adresse aux éditeurs, prestataires de services, collectivités locales et
**utilisateurs d’open source au sens large**.


.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.716814696788789%2C45.1472737206082%2C5.720355212688446%2C45.148945982622&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.14811/5.71858">Afficher une carte plus grande</a></small>


.. figure:: images/marie_face_loin.webp

   Devant la mairie, vue sur la fresque

.. figure:: images/fresque.webp

   La fresque `La liberté guidant le peuple <https://fr.wikipedia.org/wiki/La_Libert%C3%A9_guidant_le_peuple>`_

.. figure:: images/affiche_foss.webp

   La grande affiche devant la mairie annonçant AlpOSS 2024

.. figure:: images/belledonne.webp

   La place des cinq fontaines et dans le fond le massif de Belledonne en face de la mairie d'Echirolles

.. figure:: images/le_moucherotte.webp

   Le Moucherotte https://fr.wikipedia.org/wiki/Le_Moucherotte

.. figure:: images/accueil.webp

   L'accueil de la mairie d'Echirolles

.. figure:: images/hall.webp

   Dans le hall à 13h30


.. toctree::
   :maxdepth: 5

   03/03
